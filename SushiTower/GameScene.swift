//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity
import Firebase
import FirebaseFirestore

class GameScene: SKScene, WCSessionDelegate {
    
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")

    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition = "left"
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    let scoreLabel = SKLabelNode(text:"Score: ")
    
    let scoreBoard = SKLabelNode(text:"Board")
    
    var lives = 5
    var score = 0
    
    var scheduleTimer = Timer()
    
    var timer = SKSpriteNode(imageNamed: "life")
    var bg_timer = SKSpriteNode(imageNamed: "life_bg")
    
    var countTimer = 30
    
    var powerUpCount = 0
    
    var db: Firestore!
    
    
    override func sceneDidLoad() {
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        else {
            print("Phone does not support WCSession")
        }
        
        db = Firestore.firestore()
    }
    
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    
    override func didMove(to view: SKView) {
        
        scheduleTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector(("increaseTimer")), userInfo: nil, repeats: true)
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // Game labels
        self.scoreLabel.position.x = 75
        self.scoreLabel.position.y = size.height - 50
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 20
        addChild(scoreLabel)
        
        // Life label
        self.lifeLabel.position.x = 75
        self.lifeLabel.position.y = size.height - 75
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 20
        addChild(lifeLabel)
        
        self.scoreBoard.position.x = size.width - 50
        self.scoreBoard.position.y = size.height - 75
        self.scoreBoard.fontName = "Avenir"
        self.scoreBoard.fontSize = 20
        addChild(scoreBoard)
        
        
        self.timer.position.x = 30
        self.timer.position.y = size.height - 140
        self.timer.zPosition = 3
        addChild(timer)
        
        
//        self.bg_timer.position.x = 50
//        self.bg_timer.position.y = size.height - 130
//        self.bg_timer.zPosition = 1
//        addChild(bg_timer)
        
        self.timer.anchorPoint = CGPoint(x: 0,y: 0)
        //self.bg_timer.anchorPoint = CGPoint(x: 0,y: 0)
        
    
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    @objc func increaseTimer(){
        self.countTimer = countTimer - 1

        
        if (self.countTimer == 0){
            scheduleTimer.invalidate()
        }else {
            self.timer.size.width = self.timer.size.width - (self.timer.size.width / 25)
        }
        
        
        if (self.countTimer == 15 || self.countTimer == 10 || self.countTimer == 5 || self.countTimer == 0){
            sendTheTimerValueToWatch()
        }
        
            if (self.countTimer == 8 || self.countTimer == 20){
                sendPowerUpNotificationToWatch()
                powerUpCount = powerUpCount + 1;
            }
    }
    
    func sendTheTimerValueToWatch() {
        if (WCSession.default.isReachable) {
            let message = ["timer": countTimer,"type": "0"] as [String : Any]
            WCSession.default.sendMessage(message as [String : Any], replyHandler: nil)
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
        }
    }
    
    func sendPowerUpNotificationToWatch(){
        if (WCSession.default.isReachable) {
            let message = ["powerup": "powerUp", "type": "1"]
            WCSession.default.sendMessage(message as [String : Any], replyHandler: nil)
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        if self.scoreBoard.contains(mousePosition) {
            self.view?.window?.rootViewController?.performSegue(withIdentifier: "secondVC", sender: self)
        }
        
        if(countTimer > 0){

        print(mousePosition)
        removeSushi()
        
        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            
            moveCatLeft()
        }
        else {
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            
            moveCatRight()
            
        }

        punchAnimation()
    
        
        calculateScore()
            
        }
        
        
    }
    
    
    func moveCatLeft() {
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        
        // change the cat's direction
        let facingRight = SKAction.scaleX(to: 1, duration: 0)
        self.cat.run(facingRight)
        
        // save cat's position
        self.catPosition = "left"
    }
    
    func moveCatRight() {
        cat.position = CGPoint(x:self.size.width*0.85, y:100)
        
        // change the cat's direction
        let facingLeft = SKAction.scaleX(to: -1, duration: 0)
        self.cat.run(facingLeft)
        
        // save cat's position
        self.catPosition = "right"
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            
            let type = message["type"] as! String
            
            if (type == "0"){
                let movement = message["movement"] as! String
                
                if (movement == "left") {
                    if (self.countTimer > 0){
                        self.removeSushi()
                        self.moveCatLeft()
                        self.punchAnimation()
                        self.calculateScore()
                    }
                }else if (movement == "right"){
                    if (self.countTimer > 0){
                        self.removeSushi()
                        self.moveCatRight()
                        self.punchAnimation()
                        self.calculateScore()
                    }
                }
            }else if(type == "1"){
                self.countTimer = self.countTimer + 10
                self.timer.size.width = self.timer.size.width + (self.timer.size.width / 25 * 10)
            }else if(type == "2"){
                self.countTimer = 0
                print("swipe detected")
            }else if(type == "3"){
                let name = message["name"] as! String
                self.sendDataToFirebase(name: name)
            }
            
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }
    
    
    func punchAnimation() {
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
    }
    
    func removeSushi(){
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
    }
    
    func calculateScore() {
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")
                
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
            }
        }
            
        else {
            print("Sushi tower is empty!")
        }
    }
    
    
    func sendDataToFirebase(name: String){
        var ref: DocumentReference? = nil
        ref = db.collection("scores").addDocument(data: [
            name: self.score
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
}
