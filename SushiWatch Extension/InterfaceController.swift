//
//  InterfaceController.swift
//  SushiWatch Extension
//
//  Created by Melvin John on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    
    @IBOutlet weak var moveLeft: WKInterfaceButton!
    
    
    @IBOutlet weak var moveRight: WKInterfaceButton!
    
    
    @IBOutlet weak var powerUpButton: WKInterfaceButton!
    
    
    @IBOutlet weak var swipeButton: WKInterfaceButton!
    
    
    @IBOutlet weak var swipeGesture: WKSwipeGestureRecognizer!
    
    var timer = Timer()
    
    var userResponse: String!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    
    
    @IBAction func sendMoveLeft() {
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["movement" : "left","type": "0"],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
        
    }
    

    @IBAction func sendMoveRight() {
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["movement" : "right","type": "0"],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        
        let type = message["type"] as! String
        
        if (type == "0"){
            let messageBody = message["timer"] as! Int
            showWatchAlert(timer: messageBody)
        }else if (type == "1"){
            self.powerUpButton.setHidden(false)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000)) {
                self.powerUpButton.setHidden(true)
            }
            
        }
        
    }
    
    
   
    

    
    
    func showWatchAlert(timer: Int){
        if (timer == 0){
            self.presentAlert(withTitle: "Status", message: "Game Over", preferredStyle: .alert, actions: [WKAlertAction(title: "OK", style: .default){
                let suggestedResponses = ["Melvin", "John", "Jackson"]
                self.presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
                    
                    
                    if (results != nil && results!.count > 0) {
                        // 2. write your code to process the person's response
                        self.userResponse = results?.first as? String
                        
                        self.sendNameToPhone(name: self.userResponse)
                    }
                }
                }])
        }else {
            self.presentAlert(withTitle: "Status", message: "You have \(timer) seconds " , preferredStyle: .alert, actions: [WKAlertAction(title: "OK", style: .default){
                
                }])
        }
    }
    
    
    @IBAction func clickPowerUp() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["powerup" : "accepted", "type": "1"],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    func sendNameToPhone(name: String){
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["name" : name, "type": "3"],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    
    
    @IBAction func swipeRecognized(_ sender: Any) {
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["swipe" : "accepted", "type": "2"],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
    }
    
}
